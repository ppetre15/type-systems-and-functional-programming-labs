module Basics where  -- (10p)

{-
    1. (1p)
    Implement the 'reverse' function, which returns a given list
    in reverse order, using explicit recursion.
    Do NOT employ any higher-order functions.
-}

reverseRec1 :: [a] -> [a]
reverseRec1 [] = []
reverseRec1 (x:l) = (reverseRec1 l) ++ [x]

{-
    2. (1p)
    Same as (1), but change the direction on which the output is built.
    For example, if (1) built the list on return from recursion,
    you should now built the list when recursing forward.
-}

reverseRec2 :: [a] -> [a]
reverseRec2 l = reverseRec2Acc l []

reverseRec2Acc :: [a] -> [a] -> [a]
reverseRec2Acc [] l = l
reverseRec2Acc (x:l) l2 = reverseRec2Acc l (x:l2)

{-
    3. (1.5p)
    Same as (1), but use a higher-order function instead.
    Do NOT employ explicit recursion.
    Make sure that your solution faithfully reflects the process from (1).
-}

reverseHO1 :: [a] -> [a]
reverseHO1 l = foldr (\x acc -> acc ++ [x]) [] l 

{-
    4. (1.5p)
    Same as (2), but use a higher-order function instead.
    Do NOT employ explicit recursion.
    Make sure that your solution faithfully reflects the process from (2).
-}

reverseHO2 :: [a] -> [a]
reverseHO2 l = foldl (\acc x -> x : acc) [] l

{-
    5. (1p)
    Implement the power set function, which returns the set of all subsets
    of a set, using explicit recursion.
-}

powerSetRec :: [a] -> [[a]]
powerSetRec [] = [[]]
powerSetRec (x:l) = (my_map x before) ++ before 
                        where before = powerSetRec l

my_map :: a -> [[a]] -> [[a]]
my_map _ [] = []
my_map x [[]] = [[x]]
my_map x (l:ll) = [(x:l)] ++ (my_map x ll)

{-
    6. (1.5p)
    Same as (5), but use higher-order functions instead.
    Do NOT employ explicit recursion.
    Make sure that your solution faithfully reflects the process from (5).
-}

powerSetHO :: [a] -> [[a]]
powerSetHO l = foldr (\x acc -> (map (x:) acc) ++ acc) [[]] l

{-
    7. (0.5p)
    Compute the cartesian product of two lists, using list comprehensions.
-}

cartesian2 :: [a] -> [b] -> [(a, b)]
cartesian2 l l2 = [(x,y)| x <-l , y <- l2 ]

{-
    8. (2p)
    Similar to (7), but extend to any number of lists.
-}

cartesian :: [[a]] -> [[a]]
cartesian [] = [[]]
cartesian [l,l2] = [[x,y]| x <- l , y <-l2]
cartesian (l:ll) = [[x]++ y | x <-l , y <- (cartesian ll)]    