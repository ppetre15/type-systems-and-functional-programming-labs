{-# OPTIONS_GHC -F -pgmF htfpp #-}
module EvaluationTest where

import Evaluation
import qualified Evaluation.Normal as EN
import qualified Evaluation.Applicative as EA
import Util
import Grammar (parseProgram)
import Test.Framework

test_evalBig :: IO ()
test_evalBig = do
    let context = makeContext [] in do
        assertEqual (context, "x") $ evalBigInternal EN.eval context "x"
        assertEqual (context, "x") $ evalBigInternal EA.eval context "x"
    let context = makeContext [("x", "y"), ("y", "z")] in do
        assertEqual (context, "z") $ evalBigInternal EN.eval context "x"
        assertEqual (context, "z") $ evalBigInternal EA.eval context "x"
    let context = makeContext [("id", "\\x.x")] in do
        assertEqual (context, "\\y.y")
            $ evalBigInternal EN.eval context "(id \\y.y)"
        assertEqual (context, "\\y.y")
            $ evalBigInternal EA.eval context "(id \\y.y)"
    let context = makeContext [] in do
        assertEqual (context, "\\y#.((\\x.x \\x.y) y#)")
            $ evalBigInternal EN.eval context "(\\x.\\y.(x y) (\\x.x \\x.y))"
        assertEqual (context, "\\y#.(\\x.y y#)")
            $ evalBigInternal EA.eval context "(\\x.\\y.(x y) (\\x.x \\x.y))"
  where
    evalBigInternal evalSmall context expr
        = fmap show $ evalBig evalSmall context $ internal expr

test_evalList :: IO ()
test_evalList =
    let initialContext = makeContext []
        finalContext   = makeContext [("idx", "\\x.x"), ("idy", "\\y.y")]
    in assertEqual (finalContext, ["\\x.x", "\\y.y", "\\y.y"])
        $ evalListInternal EN.eval initialContext "idx=\\x.x \n idy=\\y.y \n (idx idy)"
  where
    evalListInternal evalSmall context exprs
        = fmap (map show) $ evalList evalSmall context $ parseProgram exprs