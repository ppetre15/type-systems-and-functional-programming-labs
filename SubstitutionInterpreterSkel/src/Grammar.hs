module Grammar where

import Data.Char
import Parser
import Expression

var :: Parser Char Expression
var = spotWhile1 isLetter `transform` Var

convertToFunc :: (Char, ([Char], (Char, Expression))) -> Expression
convertToFunc (x, (l, (y, expr))) = Func l expr

func :: Parser Char Expression
func = ((token '\\') >*> (spotWhile1 isLetter) >*> (token '.') >*> expr_parser ) `transform`  convertToFunc

convertToDefines :: (Expression, (Char , Expression)) -> Expression
convertToDefines (val, (x, expr)) = Defines val expr

defines :: Parser Char Expression
defines = (var >*> (token '=') >*> expr_parser) `transform` convertToDefines

convertToApplications :: (Char, (Expression, (Expression, Char))) -> Expression
convertToApplications (x, (expr1, (expr2, z))) = Applications expr1 expr2

app :: Parser Char Expression
app = ((token '(') >*> expr_parser >*> expr_parser >*> (token ')')) `transform` convertToApplications

convertFinalParser :: ([Char], (Expression, [Char])) -> Expression
convertFinalParser (s, (e, s2)) = e

isSpaceOrNewline :: Char -> Bool
isSpaceOrNewline '\n' = True
isSpaceOrNewline ' ' = True
isSpaceOrNewline '\t' = True
isSpaceOrNewline _ = False

expr_parser :: Parser Char Expression
expr_parser = ((spotWhile0 isSpaceOrNewline) >*> (app  `alt` func `alt` defines  `alt` var  ) >*> (spotWhile0 isSpaceOrNewline)) `transform`
                            convertFinalParser
                              
parseProgram :: String -> [Expression]
parseProgram text = (map fst(map head (map expr_parser (filter (/= "") (lines text)))))

 