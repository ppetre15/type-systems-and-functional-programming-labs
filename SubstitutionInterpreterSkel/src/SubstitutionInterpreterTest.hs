{-# OPTIONS_GHC -F -pgmF htfpp #-}
module Main where


import Test.Framework

import {-@ HTF_TESTS @-} VariablesTest
import {-@ HTF_TESTS @-} SubstitutionTest
import {-@ HTF_TESTS @-} Evaluation.NormalTest
import {-@ HTF_TESTS @-} Evaluation.ApplicativeTest
import {-@ HTF_TESTS @-} EvaluationTest

main :: IO ()
main = htfMain htf_importedTests
