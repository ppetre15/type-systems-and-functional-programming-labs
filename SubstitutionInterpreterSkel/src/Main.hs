module Main where

import Grammar (parseProgram)
import Evaluation
import qualified Evaluation.Normal as EN
import qualified Evaluation.Applicative as EA
import Data.Map (empty)

main :: IO ()
main = do
    program <- readFile "prog.txt"
    putStrLn $ unlines $ map show $ snd
        -- Alternatively, use 'EA.eval' for applicative-order evaluation.  
        $ evalList EN.eval empty $ parseProgram program