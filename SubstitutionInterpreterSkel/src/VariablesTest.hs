{-# OPTIONS_GHC -F -pgmF htfpp #-}
module VariablesTest where

import Variables
import Util
import Test.Framework

test_freeVars :: IO ()
test_freeVars = do
    assertListsEqualAsSets ["x"] $ freeVarsInternal "x"
    assertListsEqualAsSets [] $ freeVarsInternal "\\x.x"
    assertListsEqualAsSets ["y"] $ freeVarsInternal "\\x.y"
    assertListsEqualAsSets ["x", "y"] $ freeVarsInternal "(x y)"
    assertListsEqualAsSets ["x", "y", "z"] $ freeVarsInternal "((x y) z)"
    assertListsEqualAsSets [] $ freeVarsInternal "(\\x.x \\y.y)"
    assertListsEqualAsSets ["x"] $ freeVarsInternal "(\\x.x x)"
    assertListsEqualAsSets [] $ freeVarsInternal "\\x.(x x)"
  where
    freeVarsInternal = freeVars . internal