{-# OPTIONS_GHC -F -pgmF htfpp #-}
module SubstitutionTest where

import Substitution
import Util
import Test.Framework

test_subst :: IO ()
test_subst = do
    -- Replace x with z in x
    assertEqual "z" $ substInternal "x" "z" "x"
    assertEqual "y" $ substInternal "x" "z" "y"
    assertEqual "\\x.x" $ substInternal "x" "z" "\\x.x"
    assertEqual "\\y.z" $ substInternal "x" "z" "\\y.x"
    assertEqual "\\y#.y" $ substInternal "x" "y" "\\y.x"
    assertEqual "(y y)" $ substInternal "x" "y" "(x x)"
    assertEqual "\\y#.(y# y)" $ substInternal "x" "y" "\\y.(y x)"
    assertEqual "\\y#.(y \\y#.y)" $ substInternal "x" "y" "\\y.(x \\y.x)"
  where
    substInternal var new expr = show $ subst var (internal new) (internal expr)