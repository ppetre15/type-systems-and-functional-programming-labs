module Util where

import Expression
import Grammar (parseProgram)
import qualified Data.Map as M

internal :: String -> Expression
internal = head . parseProgram

makeContext :: [(String, String)] -> Context
makeContext = M.fromList . map (fmap internal)
