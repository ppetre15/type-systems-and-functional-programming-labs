{-# OPTIONS_GHC -F -pgmF htfpp #-}
module ContextsTest where

import Contexts
import Evaluation
import Util
import Test.Framework

test_eval :: IO ()
test_eval = do
    -- EvalVar
    assertEqual (empty, "x") $ evalInternal empty empty "x"
    assertEqual (empty, "y") $ evalInternal localX empty "x"
    assertEqual (globalX, "z") $ evalInternal empty globalX "x"
    assertEqual (globalX, "y") $ evalInternal localX globalX "x"
    -- Close
    assertEqual (empty, "<\\*x.x ; >") $ evalInternal empty empty "\\x.x"
    -- Functional closures as values
    assertEqual (empty, "<\\*x.x ; >")
        $ fmap show $ eval empty empty
        $ snd $ eval empty empty $ internal "\\x.x"
    assertEqual (globalX, "<\\*x.x ; x=y, y=z>")
        $ evalInternal localX globalX "\\x.x"
    -- EvalApp
    assertEqual (empty, "(<\\*x.x ; > \\y.y)")
        $ evalInternal empty empty "(\\x.x \\y.y)"
    -- Reduce
    assertEqual (empty, "<x ; x=<\\y.y ; >>")
        $ fmap show $ eval empty empty
        $ snd $ eval empty empty $ internal "(\\x.x \\y.y)"
    assertEqual (empty, "<x ; a=b, x=<\\y.y ; x=y, y=z>>")
        $ fmap show $ eval localX empty
        $ snd $ eval localA empty $ internal "(\\x.x \\y.y)"
    -- EvalCls
    assertEqual (empty, "<<\\y.y ; > ; x=<\\y.y ; >>")
        $ fmap show $ eval localX empty
        $ snd $ eval empty empty  -- "<x ; x=<\\y.y ; >>"
        $ snd $ eval empty empty $ internal "(\\x.x \\y.y)"
    -- Strip
    assertEqual (empty, "<\\y.y ; >")
        $ fmap show $ eval empty empty
        $ snd $ eval empty empty  -- "<<\\y.y ; > ; x=<\\y.y ; >>"
        $ snd $ eval empty empty  -- "<x ; x=<\\y.y ; >>"
        $ snd $ eval empty empty $ internal "(\\x.x \\y.y)"
  where
    evalInternal local global expr
        = fmap show $ eval local global $ internal expr
    empty   = makeContext []
    localX  = makeContext [("x", "y"), ("y", "z")]
    localA  = makeContext [("a", "b")]
    globalX = makeContext [("x", "z")]

test_evalBig :: IO ()
test_evalBig =
    let global = makeContext []
        local  = makeContext [("a", "\\z.z")]
    in assertEqual (global, "<\\*y.(y x) ; a=\\z.z, x=<a ; a=\\z.z>>")
        $ evalBigInternal (eval local) global
                          "(\\x.(\\x.x \\y.(y x)) a)"
  where
    evalBigInternal evalSmall context expr
        = fmap show $ evalBig evalSmall context $ internal expr