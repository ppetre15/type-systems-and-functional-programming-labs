{-# OPTIONS_GHC -F -pgmF htfpp #-}
module Evaluation.ApplicativeTest where

import Evaluation.Applicative
import Util
import Test.Framework

test_eval :: IO ()
test_eval = do
    let context = makeContext [] in
        assertEqual (context, "x") $ evalInternal context "x"
    let context = makeContext [("x", "y")] in
        assertEqual (context, "y") $ evalInternal context "x"
    let context = makeContext [] in
        assertEqual (context, "\\x.x") $ evalInternal context "\\x.x"
    let context = makeContext [] in
        assertEqual (context, "\\y.y") $ evalInternal context "(\\x.x \\y.y)"
    let context = makeContext [("id", "\\x.x")] in
        assertEqual (context, "(\\x.x y)") $ evalInternal context "(id y)"
    -- Functional normal form
    let context = makeContext [] in
        assertEqual (context, "\\x.(\\x.x x)")
            $ evalInternal context "\\x.(\\x.x x)"
    let (cont, _) = evalInternal (makeContext []) "id=\\x.x" in
        assertEqual (makeContext [("id", "\\x.x")]) cont
    -- Something specific to applicative-order, as opposed to normal-order
    let context = makeContext [] in
        assertEqual (context, "(\\x.y (\\x.(x x) \\x.(x x)))")
            $ evalInternal context "(\\x.y (\\x.(x x) \\x.(x x)))"
  where
    evalInternal context expr = fmap show $ eval context $ internal expr
