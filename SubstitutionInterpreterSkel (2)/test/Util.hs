module Util where

import Expression
import Type
import Grammar (parseProgram)
import qualified Data.Map as M

internal :: String -> Expression
internal = head . parseProgram

makeContext :: [(String, String)] -> Context
makeContext = M.fromList . map (fmap internal)

makeSubst :: [(String, String)] -> Substitution
makeSubst = M.fromList . map (fmap read)