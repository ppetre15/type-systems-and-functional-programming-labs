Files:
* Parser.hs: the parsing library
* Expression.hs: TODO: the internal expression representation
* Grammar.hs: TODO: the language grammar
* Variables.hs: TODO: free variables operator
* Substitution.hs: TODO: substitution rules
* Evaluation/Normal.hs: TODO: rules for small-step normal-order evaluation
* Evaluation/Applicative.hs: TODO: rules for small-step applicative-order evaluation
* Evaluation.hs: TODO: big-step evaluation of single expressions and expression lists
* Contexts.hs: TODO: rules for small-step contextual-evaluation.
* Type.hs: internal type representation
* Unification.hs: TODO: type unification
* Main.hs: main module
* prog.txt: sample program

See the tests for use cases.

The tests make the following assumptions:
* The data type that encodes lambda expressions instantiates the 'Show' class.
* The Grammar module exposes the 'parseProgram' function, which takes
  a program string and returns a list of internal expression representations.
* The Variables module exposes the 'freeVars' function, which returns
  the list of free variable names within a given expression.
* The Substitution module exposes the 'subst' function, which encodes
  the substitution rules.
* In case of conflict, the 'subst' function renames the bound variable
  by appending a '#' to its name.