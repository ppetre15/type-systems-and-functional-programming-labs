module Evaluation.Applicative where

import Expression
import Substitution
import qualified Data.Map as M

{-
    Small-step evaluation of a given expression, within a given context.
    data Expression = Var String
        | Func String Expression
        | Defines Expression Expression
        | Applications Expression Expression deriving Eq
-}
eval :: Context                -- The context where the evaluation takes place
     -> Expression             -- The expression to be evaluated
     -> (Context, Expression)  -- The evaluation result,
                               -- along with a possibly enriched context
                               -- in case of a definition
eval context (Var s) = (context,(substition (M.toList context) (Var s)))
eval context (Func s expr) = (context,(substition (M.toList context) (Func s expr)))
eval context (Defines e1 e2) = ((insertIntoContext context e1 e2),e2)
eval context (Applications e1 e2) = if ((M.toList context)==[]) then (context,(apply_func e2 e1))
	else if ((substition (M.toList context) (Applications e1 e2))==(Applications e1 e2)) then (context,(apply_func e2 e1))
	else (context, (substition (M.toList context) (Applications e1 e2)))	
substition :: [(String,Expression)]
			-> Expression
			-> Expression
substition [] e = e
substition ((s,e1):list) e2 = substition list (subst s e1 e2)

insertIntoContext :: Context
				-> Expression
				-> Expression
				-> Context
insertIntoContext context (Var s) e = M.insert s e context

apply_func :: Expression
		   -> Expression
		   -> Expression
apply_func (Var s) (Func x e) = subst x (Var s) e
apply_func (Func s expr) (Func x e) = subst x (Func s expr) e
apply_func (Applications e1 e2) (Func x e) = (Applications (Func x e) (apply_func e2 e1))