module Variables where

import Expression
import Grammar
import qualified Data.Set as Set  

freeVars :: Expression -> [String]
freeVars (Var x) = [x]
freeVars (Defines x expr) = (freeVars x) ++ (freeVars expr)
freeVars (Func x expr) = Set.toList (filterBoundVars expr (Set.fromList [x]) (Set.empty))
freeVars (Applications expr1 expr2) = (freeVars expr1) ++ (freeVars expr2) 

filterBoundVars :: Expression -> Set.Set String -> Set.Set String  -> Set.Set String
filterBoundVars (Var x)  bound free = if (Set.fromList [x] `Set.isSubsetOf` bound) then free
																			else Set.insert x free
filterBoundVars (Func x expr) bound free = filterBoundVars expr (Set.insert x bound) free
filterBoundVars (Applications expr1 expr2) bound free = (filterBoundVars expr1 bound free) `Set.union` (filterBoundVars expr2 bound free)


 