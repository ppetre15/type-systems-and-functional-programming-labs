module Expression where

import Data.Map


data Expression = Var String
        | Func String Expression
        | Defines Expression Expression
        | Applications Expression Expression deriving Eq

instance Show Expression where
    show (Var x) = x 
    show (Func name expr) = "\\" 	++ name ++"."++ show(expr)
    show (Defines name expr) = show(name) ++"="++ show(expr)
    show (Applications expr1 expr2) = "(" ++ show(expr1) ++" "++ show(expr2)++")"

type Context = Map String Expression
