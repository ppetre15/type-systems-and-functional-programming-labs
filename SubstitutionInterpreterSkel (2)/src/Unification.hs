module Unification where

import Type
import qualified Data.Map as M
import qualified Data.Maybe as MM 


{-|
    Obtains the end of the binding chain for the given type.
    The search ends when either of the following is reached:
    
    * an unbound type variable
    
    * a function type.
-}
chainEnd :: Substitution  -- ^ Substitution
         -> Type          -- ^ Type to look up
         -> Type          -- ^ Chain end
chainEnd sub (TypeVar s) = if (result == (TypeVar s)) then (TypeVar s)
                 else chainEnd sub result where
                 result = M.findWithDefault (TypeVar s) s sub

chainEnd sub (Arrow t t2) = (Arrow t t2) 


{-|
    Returns true if a type variable does NOT appear in the given type.
    The type variable is assumed FREE within the substitution.
-}
occCheck :: Substitution  -- ^ Substitution
         -> String        -- ^ Type variable to check for occurrence
         -> Type          -- ^ Type to look in
         -> Bool          -- ^ True if the type variable does NOT occur
occCheck subst s (TypeVar t) = if (s == t) then False
                               else if (result == TypeVar t) then True
                                else occCheck subst s result where
                                  result = (chainEnd subst (TypeVar t))
occCheck subst s (Arrow t t2) = if ((occCheck subst s t)==False) then False 
                                else occCheck subst s t2
{-
    Unifies two type expressions.
-}
unify :: Substitution        -- ^ Initial substitution
      -> Type                -- ^ First type
      -> Type                -- ^ Second type
      -> Maybe Substitution  -- ^ The MGU of the two types,
                             --   or Nothing, if they cannot unify
unify subst (TypeVar a) (TypeVar b) = if (ra == rb) then Just subst
                                      else Just (M.insert (show ra) rb subst)
                                     where 
                                     ra = chainEnd subst (TypeVar a)
                                     rb = chainEnd subst (TypeVar b)
unify subst (TypeVar a) (Arrow t1 t2) = if (occCheck subst (show ra) (Arrow rt1 rt2)) then Just (M.insert (show ra) (Arrow t1 t2) subst)
                                            else Nothing
                                        where 
                                        ra = chainEnd subst (TypeVar a)
                                        rt1 = chainEnd subst t1
                                        rt2 = chainEnd subst t2
unify subst (Arrow t1 t2) (Arrow t3 t4) = if (u1 == Nothing) then Nothing
                                              else if (u2 == Nothing) then Nothing
                                              else Just (M.union (MM.fromMaybe  M.empty u1) (MM.fromMaybe M.empty u2))
                                              where
                                              u1 = unify subst t1 t3
                                              u2 = unify subst t2 t4 

{-|
    Applies a substitution to a type expression.
-}
applySubst :: Substitution  -- ^ Substitution to be applied
           -> Type          -- ^ Target type
           -> Type          -- ^ Resulting type
applySubst subst (TypeVar tp) = let chEnd = chainEnd subst (TypeVar tp)
                  in if (not ((TypeVar tp) == chEnd)) then applySubst subst chEnd
                    else chEnd

applySubst subst (Arrow t1 t2) = (Arrow (applySubst subst t1) (applySubst subst t2))