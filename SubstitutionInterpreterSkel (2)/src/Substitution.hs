module Substitution where

import Expression
import Variables
import Grammar

subst :: String      -- Variable
      -> Expression  -- New expression
      -> Expression  -- Existing expression
      -> Expression  -- Resulting expression
subst z expr1 expr2 = if (member (freeVars expr2) z) then change2 z expr1 (change (show expr1) expr2)
													else expr2

change3 :: String 
		-> Expression
		-> Expression
change3 t (Var z) = if (z == t) then (Var (t++"#"))
						else Var z
change3 t (Defines expr1 expr2) = (Defines (change3 t expr1) (change3 t expr2))
change3 t (Func s expr2) = if (s == t) then (Func (s++"#") (change3 t expr2))
						else (Func s expr2)
change3 t (Applications expr1 expr2) = (Applications (change3 t expr1) (change3 t expr2))

change2 :: String 		-- var to change
		-> Expression  -- New expression
	   -> Expression    -- old expression 
	   -> Expression 	-- Resulting expression

change2 z expr1 (Var t) = if (z == t) then expr1
							  		else (Var t)
change2 z expr (Defines expr1 expr2) =  Defines (change2 z expr expr1) (change2 z  expr expr2)
change2 z expr1 (Func t expr2) = if z == t then Func (show expr1) (change2 z expr1 expr2)
									else if (member (freeVars expr1) t) then Func (t++"#") (change2 z expr1 (change3 t expr2))
										else Func t (change2 z expr1 expr2)

change2 z expr (Applications expr1 expr2) = Applications (change2 z expr expr1) (change2 z expr expr2)

change :: String 		-- var to change
	   -> Expression    -- old expression 
	   -> Expression 	-- Resulting expression
change z (Var t) = if z == t then Var (z ++"#")
							  else Var t
change z (Defines expr1 expr2) =  Defines (change z expr1) (change z expr2)
change z (Func t expr2) = if z == t then Func (z++"#") (change z expr2)
									else Func t (change z expr2)
change z (Applications expr1 expr2) = Applications (change z expr1) (change z expr2)

member [] y = False
member (x:xs) y = (x == y) || (member xs y)