{-# OPTIONS_GHC -F -pgmF htfpp #-}
module InferenceTest where

import Inference
import Type
import Util
import qualified Data.Set as S
import Test.Framework

vars :: Type -> S.Set String
vars (TypeVar x) = S.singleton x
vars (Arrow t1 t2) = vars t1 `S.union` vars t2

test_copy :: IO ()
test_copy = do
    assertBool $ check "a"
    assertBool $ check "(a->a)"
    assertBool $ check "(a->b)"
    assertBool $ check "(a->(a->a))"
    assertBool $ check "(a->(b->c))"
  where
    check t = t1 ~= t2 && S.null (vars t1 `S.intersection` vars t2)
      where
        t1      = read t
        (_, t2) = copy 0 t1

test_infer :: IO ()
test_infer = do
    assertBool $ "a" @~= inferInternal emptyContext emptyContext "x"
    assertEqual (read "a") $ inferInternal contextX emptyContext "x"
    let t = inferInternal emptyContext contextX "x" in do
        assertBool $ "a" @~= t
        assertNotEqual (TypeVar "a") t
  where
    inferInternal local global expr =
        let Right (_, _, _, typ) = infer local global emptySubst 0 $ internal expr
        in  typ
    emptyContext = makeTContext []
    contextX     = makeTContext [("x", "a")]
    emptySubst   = makeSubst []