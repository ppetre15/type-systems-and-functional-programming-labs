module Type where

import Parser
import Data.Char
import qualified Data.Map as M

data Type
    = TypeVar String   -- Type variable
    | Arrow Type Type  -- Function type
    deriving Eq

instance Show Type where
    show (TypeVar v)   = v
    show (Arrow t1 t2) = "(" ++ show t1 ++ "->" ++ show t2 ++ ")"

instance Read Type where
    readsPrec _ = typ
      where
        typ = typeVar `alt` arrow
        typeVar = spotWhile1 isAlphaNum `transform` TypeVar
        arrow = (token '(' >*> typ
             >*> token '-' >*> token '>' >*> typ >*> token ')')
            `transform` \(_, (t1, (_, (_, (t2, _))))) -> Arrow t1 t2

-- Binds TYPE variables to types
type Substitution = M.Map String Type

-- Binds PROGRAM variables to types
type TypingContext = M.Map String Type

-- Counter for generated type variables
type Counter = Int