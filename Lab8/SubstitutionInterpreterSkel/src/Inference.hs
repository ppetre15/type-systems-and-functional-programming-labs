module Inference where

import Expression
import Type
import qualified Data.Map as M
import qualified Data.Maybe as MM
import Unification

{-|
    Generates a unique copy of the given type
-}

isLeft  = either (const True) (const False)
isRight = either (const False) (const True)

copy :: Counter
     -> Type
     -> (Counter, Type)

copy cnt tp = (cnt1, tp1) where
              (cnt1, tp1, mp1) = (copyInt cnt tp M.empty)

--mp = dictionary containing the new names of the vars
copyInt cnt (TypeVar tp) mp = if (MM.Nothing == newTp) then 
                                ((cnt + 1), (TypeVar ("t" ++ (show cnt))), (M.insert tp ("t" ++ (show cnt)) mp))
                            else (cnt, (TypeVar (MM.fromJust newTp)), mp)
                            where newTp = (M.lookup tp mp)

copyInt cnt (Arrow t1 t2) mp = (cnt2, (Arrow tp11 tp21), mp2) 
                where 
                (cnt1, tp11, mp1) = (copyInt cnt t1 mp)
                (cnt2, tp21, mp2) = (copyInt cnt1 t2 mp1)
                  

generateType cnt = (TypeVar ("tp" ++ show cnt))


infer :: TypingContext  -- ^ Lexical context
      -> TypingContext  -- ^ Dynamic context
      -> Substitution   -- ^ Substitution
      -> Counter        -- ^ Current type variable counter
      -> Expression     -- ^ Expression to type
      -> Either String (TypingContext, Substitution, Counter, Type)
                        -- ^ If the typing succeeds,
                        --   the inferred type, and possibly updated versions
                        --   of the arguments; otherwise, an error message.

infer locCtx dynCtx subst cnt (Var a) = let locTp = (M.lookup a locCtx)
                      in if (MM.isJust locTp) then (Right (dynCtx, subst, cnt, (MM.fromJust locTp)))
                        else let dynTp = (M.lookup a dynCtx)
                            in if (MM.isJust dynTp) then let (cnt1, tp1) = (copy cnt (MM.fromJust dynTp))
                                            in (Right (dynCtx, subst, cnt1, tp1))
                              else let tp2 = ("t" ++ (show cnt))
                                    in (Right (dynCtx, subst, (cnt + 1), (TypeVar tp2)))

infer locCtx dynCtx subst cnt (Func x e)
    = let tp_x = (generateType cnt)
        in case (infer (M.insert x  tp_x locCtx) dynCtx subst (cnt + 1) e) of
            Left(_) -> (Left ("Non unify"))
            Right (dyn1, subst4, cnt4, tp_e) -> (Right (dyn1, subst4, cnt4, (Arrow tp_x tp_e)))


infer locCtx dynCtx subst cnt (Applications e1 e2) 
    = let tp1 = (generateType cnt)
          tp2 = (generateType (cnt + 1))
        in case (infer locCtx dynCtx subst (cnt + 2) e1) of 
            Left(_) -> (Left ("Non unify"))
            Right (dyn, subst3, cnt3, tp_e1) -> case (infer locCtx dyn subst3 cnt3 e2) of
                                        Left(_) -> (Left ("Non unify"))
                                        Right (dyn1, subst4, cnt4, t0) -> let  subst1 = MM.fromMaybe M.empty (unify subst4 tp_e1 (Arrow tp1 tp2))
                                                                 in if (subst1==M.empty) then (Left ("Non unify"))
                                                                        else let subst2 = (unify subst1 tp1 t0)
                                                                               in if not (subst2 == MM.Nothing) then (Right (dyn1, MM.fromMaybe M.empty subst2, cnt4, tp2))
                                                                                    else (Left ("Non unify"))

                                                         
infer locCtx dynCtx subst cnt (Defines e1 e2) 
    = case ((infer locCtx dynCtx subst cnt e1)) of 
        Left(_) -> (Left ("Non unify"))
        Right (_, subst1, cnt1, t1) -> case ((infer locCtx dynCtx subst1 cnt1 e2)) of
                                Left(_) -> (Left ("Non unify"))
                                Right (_, _, _, t2) -> let subst2 = (unify subst1 t1 t2)
                                                        in if not (subst2==MM.Nothing) then (Right (dynCtx, MM.fromMaybe M.empty subst2, cnt, t2))
                                                            else (Left ("Non unify"))

