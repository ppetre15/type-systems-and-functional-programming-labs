module Evaluation where

import Expression

{-
    Big-step evaluation of a given expression, within a given context.
    The evaluation should stop when either the value is reached,
    or the expression cannot be reduced further.
    
    The first argument is the small-step evaluation function.
-}
evalBig :: (Context -> Expression -> (Context, Expression))  -- Small-stepper
        -> Context                -- The context where the evaluation takes place
        -> Expression             -- The expression to be evaluated
        -> (Context, Expression)  -- The evaluation result,
                                  -- along with a possibly enriched context
                                  -- in case of a definition
evalBig eval context (Var s) = eval context (Var s)
evalBig eval context (Func x e) = if ((snd result)==(Func x e)) then result
                                  else evalBig eval (fst result) (snd result) where
                                  result = eval context (Func x e)
evalBig eval context (Defines e1 e2) = if ((snd result)==(Defines e1 e2)) then result
                                  else evalBig eval (fst result) (snd result) where
                                  result = eval context (Defines e1 e2)
evalBig eval context (Applications e1 e2) = if ((snd result)==(Applications e1 e2)) then result
                                  else evalBig eval (fst result) (snd result) where
                                  result = eval context (Applications e1 e2)


{-
    Big-step evaluation of a list of expressions, starting with
    the given context and using it throughout the entire list,
    for propagating the encountered definitions.
    
    The first argument is the small-step evaluation function.
-}
evalList :: (Context -> Expression -> (Context, Expression))
         -> Context
         -> [Expression]
         -> (Context, [Expression])
evalList eval context [] = (context,[])
evalList eval context (l:list) = ((fst result2),((snd result):(snd result2)))
                            where 
                              result=evalBig eval context l
                              result2=evalList eval (fst result) list