module Contexts where

import Expression
import qualified Data.Map as M

eval :: Context                -- Local context
     -> Context                -- Global context
     -> Expression             -- Expression to evaluate
     -> (Context, Expression)  -- Evaluation result,
                               -- along with a possibly enriched global context,
                               -- in case of a definition
eval = undefined